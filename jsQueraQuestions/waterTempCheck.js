// Input a number to check what`s the phase of water on that temp.!

const waterTempCheck = (t) => {
  let checkState = "";
  if (t <= 6000 && t >= -273) {
    if (t >= 0 && t <= 100) {
      checkState = "Liquid Water";
    } else if (t < 0) {
      checkState = "Ice Water";
    } else if (t > 100) {
      checkState = "Steam Water";
    }
    return checkState;
  } else {
    return "Wrong Temp.";
  }
};

console.log("Result:", waterTempCheck(150));
