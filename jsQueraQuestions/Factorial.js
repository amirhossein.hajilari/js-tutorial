//calculate Factorial Of N (between 1 to 1000)

const FactorialOf = (n) => {
  let factorial = 1;
  if ((n >= 0, n <= 1000)) {
    for (i = n; i > 0; i--) {
      factorial = factorial * i;
    }
    return factorial;
  } else {
    return "Invalid Number";
  }
};

console.log("And the Result is:", FactorialOf(10));
