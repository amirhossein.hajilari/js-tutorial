//We Have an square , input number N for lines we have, check max Squares with N Line we`d make on our square...

const getMaxSquares = (x) => {
  if (x > 0) {
    if (x % 2 == 0) {
      const maxSize = (x / 2 + 1) ** 2;
      return maxSize;
    } else {
      const ops = Math.ceil(x / 2);
      const maxSize = (ops + 1) * ops;
      return maxSize;
    }
  } else {
    return "Validation Error: The Number you choose is under 0";
  }
};

console.log(getMaxSquares(10));
